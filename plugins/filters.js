import Vue from 'vue'

Vue.filter('formatDate', (value) => {
  const date = new Date(value)
  return date.toLocaleString(['de-CH'], {
    month: '2-digit',
    weekday: 'short',
    day: '2-digit',
  })
})

Vue.filter('formatTime', (value) => {
  const date = new Date(value)
  return date.toLocaleString(['de-CH'], {
    hour: '2-digit',
    minute: '2-digit',
  })
})
